#pragma once
#ifndef SOUND_H
#define SOUND_H
#include "raylib.h"

#endif // !SOUND_H
namespace sound {
	extern Sound playerbeep;
	extern Sound bossbeep;
	extern Sound blockbeep;
	extern Music musicmenu;
	extern Music musicgame;
	extern Music musicboss;
	extern Music musicoptions;
	extern Music musiclevelselection;

	extern void initSounds();
	extern void modificarVolumen(float auxvolumen[], short input);
}