#include "Sound.h"
namespace sound {
	Sound playerbeep = LoadSound("res/sound/beep1.wav");
	Sound bossbeep = LoadSound("res/sound/bop.wav");
	Sound blockbeep = LoadSound("res/sound/beep2.wav");
	Music musicmenu = LoadMusicStream("res/sound/mainmenu.mp3");
	Music musicgame = LoadMusicStream("res/sound/game.mp3");
	Music musicboss = LoadMusicStream("res/sound/bosstheme.mp3");
	Music musicoptions = LoadMusicStream("res/sound/optionsmenu.mp3");
	Music musiclevelselection= LoadMusicStream("res/sound/levelselection.mp3");
	void initSounds() {
		InitAudioDevice();
		SetSoundVolume(playerbeep, 0.5f);
		SetSoundVolume(bossbeep, 0.5f);
		SetSoundVolume(blockbeep, 0.5f);
		SetMusicVolume(musicmenu, 0.5f);
		SetMusicVolume(musicgame, 0.5f);
		SetMusicVolume(musicboss, 0.5f);
		SetMusicVolume(musicoptions, 0.5f);
		SetMusicVolume(musiclevelselection, 0.5f);
	}
	void modificarVolumen(float auxvolumen[], short input) {
		SetSoundVolume(playerbeep, auxvolumen[input]);
		SetSoundVolume(bossbeep, auxvolumen[input]);
		SetSoundVolume(blockbeep, auxvolumen[input]);
		SetMusicVolume(musicmenu, auxvolumen[input]);
		SetMusicVolume(musicgame, auxvolumen[input]);
		SetMusicVolume(musicboss, auxvolumen[input]);
		SetMusicVolume(musicoptions, auxvolumen[input]);
		SetMusicVolume(musiclevelselection, auxvolumen[input]);
	}
}