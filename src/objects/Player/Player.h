#pragma once

#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "../../ScreenSize.h"

namespace player {
	class Player {
	private:
		Color _color;
		Rectangle _sizepos;
		Vector3 hsv;
	public:
		Player(short level, float y);
		void movementControl(short level);
		void draw();
		Color getColor();
		Rectangle getSizePos();
		float getSizePosX();
		float getSizePosY();
		float getSizePosWidth();
		float getSizePosHeight();

		void setColor(Color color);
		void setSizePos(Rectangle sizepos);
		void setSizePosX(float x);
		void setSizePosY(float y);
		void setSizePosWidth(float width);
		void setSizePosHeight(float height);
		Color bossColor(short lifes);
	};
}
#endif // !PLAYER_H
