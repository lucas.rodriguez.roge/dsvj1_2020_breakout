#include "Player.h"
namespace player {
	Player::Player(short level,float y) {
		_sizepos.x = 400;
		_sizepos.y = y;
		switch (level) {
			case 1:
				_sizepos.width = 100;
				break;
			case 2:
				_sizepos.width = 90;
				break;
			case 3:
				_sizepos.width = 80;
				break;
			case 4:
				_sizepos.width = 70;
				break;
			case 5:
				_sizepos.width = 60;
				break;
			case 6:
				_sizepos.width = 50;
				break;
			case 7:
				_sizepos.width = 45;
				break;
			case 8:
				_sizepos.width = 40;
				break;
			case 9:
				_sizepos.width = 35;
				break;
			case 10:
				_sizepos.width = 100;
				break;
		}
		hsv.x = 0;
		hsv.y = 1;
		hsv.z = 1;
		_sizepos.height = 25;
		_color = WHITE;
	}
	void Player::draw() {
		DrawRectangleRec(_sizepos, _color);

	}
	void Player::movementControl(short level) {
		if (level > 5) {
			if (IsKeyDown(KEY_RIGHT)) _sizepos.x -= 5;
			if ((_sizepos.x - _sizepos.x / 2) <= 0) _sizepos.x = 0;
			if (IsKeyDown(KEY_LEFT)) _sizepos.x += 5;
			if ((_sizepos.x + _sizepos.width) >= screensize::screenWidth) _sizepos.x = screensize::screenWidth - _sizepos.width;
		}
		else {
			if (IsKeyDown(KEY_LEFT)) _sizepos.x -= 5;
			if ((_sizepos.x - _sizepos.x / 2) <= 0) _sizepos.x = 0;
			if (IsKeyDown(KEY_RIGHT)) _sizepos.x += 5;
			if ((_sizepos.x + _sizepos.width) >= screensize::screenWidth) _sizepos.x = screensize::screenWidth - _sizepos.width;
		}
	}
	Rectangle Player::getSizePos() {
		return _sizepos;
	}
	float Player::getSizePosX() {
		return _sizepos.x;
	}
	float Player::getSizePosY() {
		return _sizepos.y;
	}
	float Player::getSizePosWidth() {
		return _sizepos.width;
	}
	float Player::getSizePosHeight() {
		return _sizepos.height;
	}
	Color Player::getColor() {
		return _color;
	}
	void Player::setColor(Color color) {
		_color = color;
	}
	void Player::setSizePos(Rectangle sizepos) {
		_sizepos = sizepos;
	}
	void Player::setSizePosX(float x) {
		_sizepos.x = x;
	}
	void Player::setSizePosY(float y) {
		_sizepos.y = y;
	}
	void Player::setSizePosWidth(float width) {
		_sizepos.width = width;
	}
	void Player::setSizePosHeight(float height) {
		_sizepos.height = height;
	}
	Color Player::bossColor(short lifes) {
		if (hsv.x < 360)
			hsv.x+=lifes;
		else for (short i = 360;i > 0;i-=lifes)hsv.x = i;
		return ColorFromHSV(hsv);
	}
}