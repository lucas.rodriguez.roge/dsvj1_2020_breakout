#pragma once

#ifndef BALL_H
#define BALL_H
#include "raylib.h"
#include "../Player/Player.h"

namespace ball{
	class Ball {
	private:

		Vector2 _speed;
		Vector2 _pos;
		double _radius;
		Color _color;
		bool _active;
	public:
		Ball();
		void draw();
		Vector2 getPos();
		double getRadius();
		float getPosX();
		float getPosY();
		Vector2 getSpeed();
		double getSpeedX();
		double getSpeedY();
		bool getActive();
		void setSpeedX(double x);
		void setSpeedY(double y);
		void setPos(Vector2 pos);
		void setPosX(float x);
		void setPosY(float y);
		void setSpeed(Vector2 speed);
		void setActive(bool active);
	};
}
#endif // !BALL_H
