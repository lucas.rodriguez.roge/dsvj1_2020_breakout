#include "Ball.h"

namespace ball {
	Ball::Ball() {
		_speed.y = 5;
		_speed.x = 5;
		_pos.x = 0;
		_pos.y = 0;
		_radius = 5.0f;
		_color = WHITE;
		_active = false;
	}
	void Ball::draw() {
		DrawCircleV(_pos, _radius, _color);
	}
	Vector2 Ball::getPos() {
		return _pos;
	}
	float Ball::getPosX() {
		return _pos.x;
	}
	float Ball::getPosY() {
		return _pos.y;
	}
	Vector2 Ball::getSpeed() {
		return _speed;
	}
	double Ball::getSpeedX() {
		return _speed.x;
	}
	double Ball::getSpeedY() {
		return _speed.y;
	}
	double Ball::getRadius() {
		return _radius;
	}
	bool Ball::getActive() {
		return _active;
	}
	void Ball::setSpeed(Vector2 speed) {
		_speed = speed;
	}
	void Ball::setPosX(float x) {
		_pos.x = x;
	}
	void Ball::setPosY(float y) {
		_pos.y = y;
	}
	void Ball::setPos(Vector2 pos) {
		_pos = pos;
	}
	void Ball::setSpeedX(double x) {
		_speed.x = x;
	}
	void Ball::setSpeedY(double y) {
		_speed.y = y;
	}
	void Ball::setActive(bool active) {
		_active = active;
	}
}
