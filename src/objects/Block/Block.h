#pragma once
#ifndef BLOCK_H
#define BLOCK_H
#include "raylib.h"

namespace block {
	class Block {
	private:
		Color _color;
		Rectangle _sizepos;
		bool _exist;
	public:
		Block(float x, float y, float width, float height, Color color);
		void draw();
		Color getColor();
		Rectangle getSizePos();
		float getSizePosX();
		float getSizePosY();
		float getSizePosWidth();
		float getSizePosHeight();
		bool getExist();
		void setExist(bool exist);
		void setColor(Color color);
		void setSizePos(Rectangle sizepos);
		void setSizePosX(float x);
		void setSizePosY(float y);
		void setSizePosWidth(float width);
		void setSizePosHeight(float height);
	};
}
#endif // !BLOCK_H
