#include "Block.h"

namespace block {
	Block::Block(float x, float y, float width, float height, Color color) {
		_sizepos.x = x;
		_sizepos.y = y;
		_sizepos.width = width;
		_sizepos.height = height;
		_color = color;
		_exist = true;
	}
	void Block::draw() {
		DrawRectangleRec(_sizepos, _color);
	}

	Color Block::getColor() {
		return _color;
	}
	Rectangle Block::getSizePos() {
		return _sizepos;
	}
	float Block::getSizePosX() {
		return _sizepos.x;
	}
	float Block::getSizePosY() {
		return _sizepos.y;
	}
	float Block::getSizePosWidth() {
		return _sizepos.width;
	}
	float Block::getSizePosHeight() {
		return _sizepos.height;
	}
	bool Block::getExist() {
		return _exist;
	}

	void Block::setColor(Color color) {
		_color = color;
	}
	void Block::setExist(bool exist) {
		_exist = exist;
	}
	void Block::setSizePos(Rectangle sizepos) {
		_sizepos = sizepos;
	}
	void Block::setSizePosX(float x) {
		_sizepos.x = x;
	}
	void Block::setSizePosY(float y) {
		_sizepos.y = y;
	}
	void Block::setSizePosWidth(float width) {
		_sizepos.width = width;
	}
	void Block::setSizePosHeight(float height) {
		_sizepos.height = height;
	}
}