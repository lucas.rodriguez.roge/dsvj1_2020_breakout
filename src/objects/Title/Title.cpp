#include "Title.h"

namespace title {
    Title::Title(string label) {
        _label = label;
    }
    void Title::draw() {
        if (_label == "Arcanoid")
            DrawText("Arcanoid", 275, 50, 60, titleColor());
    }
    Color Title::titleColor() {
        if (hsv.x < 360)
            hsv.x++;
        else for (short i = 360;i > 0;i--)hsv.x = i;
        return ColorFromHSV(hsv);
    }
    void Title::init() {
        hsv.x = 0;
        hsv.y = 1;
        hsv.z = 1;
    }
}


