#ifndef TITLE_H
#define TITLE_H

#include <iostream>
#include "raylib.h"


using namespace std;
namespace title {
    class Title {
    private:
        string _label;
        Vector3 hsv;
    public:
        Title(string label);
        void draw();
        Color titleColor();
        void init();
    };
}
#endif // !TITLE_H
