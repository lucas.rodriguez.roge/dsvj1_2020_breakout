#pragma once
#ifndef OPTIONS_H
#define OPTIONS_H
#include <iostream>
#include "raylib.h"

namespace options {
	using namespace std;
	enum class CHOISE { PLAY, OPTIONS, EXIT, RESUME, GOBACK,
	LEVEL1,LEVEL2,LEVEL3,LEVEL4,LEVEL5,LEVEL6,LEVEL7,LEVEL8,
	LEVEL9,LEVEL10,GOBACKLEVEL,CREDITS,CHARACTERSELECTION,GOBACKOPTIONS,VOLUME};

	class Options {
	private:
		string _label;
		Color _color;
		Vector2 _pos;
		Texture2D _textbox;
		Texture2D _minitextbox;
		
	public:
		Options(string label, Vector2 pos);
		void optionSelection(CHOISE& _input);
		void draw();
	};
}


#endif // !OPTIONS_H
