#include "Options.h"
namespace options {

	Options::Options(string label, Vector2 pos) {
		_label = label;
		_color = WHITE;
		_pos = pos;
		_textbox = LoadTexture("res/images/textbox2.png");
		_minitextbox = LoadTexture("res/images/mini_textbox1.png");	
	}
	void Options::optionSelection(CHOISE& _input) {	
		switch (_input) {
		case CHOISE::PLAY: {
			_color = RED;
			if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::OPTIONS;
			}
			break;
		}
		case CHOISE::OPTIONS: {
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				_color = WHITE;
				_input = CHOISE::PLAY;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::EXIT;
			}
			break;
		}
		case CHOISE::EXIT: {
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				_color = WHITE;
				_input = CHOISE::OPTIONS;
			}
			break;
		}
		case CHOISE::RESUME: {
			_color = RED;
			if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACK;
			}
			break;
		}
		case CHOISE::GOBACK: {
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				_color = WHITE;
				_input = CHOISE::RESUME;
			}
			break;
		}
		case CHOISE::LEVEL1:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL2;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL10;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL2:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL3;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL1;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL3:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL4;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL2;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL4:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL5;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL3;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL5:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL6;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL4;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL6:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL7;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL5;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL7:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL8;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL6;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL8:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL9;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL7;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL9:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL10;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL8;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::LEVEL10:
			_color = RED;
			if (IsKeyPressed(KEY_RIGHT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL1;
			}
			if (IsKeyPressed(KEY_LEFT)) {
				_color = WHITE;
				_input = CHOISE::LEVEL9;
			}
			else if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKLEVEL;
			}
			break;
		case CHOISE::GOBACKLEVEL: {
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				_color = WHITE;
				_input = CHOISE::LEVEL1;
			}
			break;
		case CHOISE::CREDITS:
			_color = RED;
			if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::CHARACTERSELECTION;
			}
			break;
		case CHOISE::CHARACTERSELECTION:
			_color = RED;
			if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::VOLUME;
			}
			if (IsKeyPressed(KEY_UP)) {
				_color = WHITE;
				_input = CHOISE::CREDITS;
			}
			break;
		case CHOISE::VOLUME:
			_color = RED;
			if (IsKeyPressed(KEY_DOWN)) {
				_color = WHITE;
				_input = CHOISE::GOBACKOPTIONS;
			}
			if (IsKeyPressed(KEY_UP)) {
				_color = WHITE;
				_input = CHOISE::CHARACTERSELECTION;
			}
			break;
		case CHOISE::GOBACKOPTIONS:
			_color = RED;
			if (IsKeyPressed(KEY_UP)) {
				_color = WHITE;
				_input = CHOISE::VOLUME;
			}
			break;
		}
		}
	}
	void Options::draw() {

		if (_label == "PLAY") {
			DrawTexture(_textbox, _pos.x - 60, _pos.y - 20, WHITE);
			DrawText("PLAY", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "OPTIONS") {
			DrawTexture(_textbox, _pos.x - 60, _pos.y - 20, WHITE);
			DrawText("OPTIONS", _pos.x-51, _pos.y, 57, _color);
		}
		else if (_label == "EXIT") {
			DrawTexture(_textbox, _pos.x - 60, _pos.y - 20, WHITE);
			DrawText("EXIT", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "RESUME") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("RESUME", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "GOBACK") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("BACK", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "1") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("1", _pos.x+10, _pos.y, 40, _color);
		}
		else if (_label == "2") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("2", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "3") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("3", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "4") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("4", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "5") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("5", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "6") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("6", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "7") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("7", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "8") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("8", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "9") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("9", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "10") {
			DrawTexture(_minitextbox, _pos.x - 5, _pos.y - 5, WHITE);
			DrawText("10", _pos.x + 10, _pos.y, 40, _color);
		}
		else if (_label == "GOBACKLEVEL") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("BACK", _pos.x + 50, _pos.y, 60, _color);
		}
		else if (_label == "CREDITS") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("CREDITS", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "PADDLE") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("PADDLE", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "VOLUME") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("VOLUME", _pos.x, _pos.y, 60, _color);
		}
		else if (_label == "GOBACKOPTIONS") {
			DrawTexture(_textbox, _pos.x - 5, _pos.y - 20, WHITE);
			DrawText("BACK", _pos.x + 50, _pos.y, 60, _color);
		}


	}

}