#include "scenes/MainMenu/MainMenu.h"

void main() {

    InitWindow(screensize::screenWidth, screensize::screenHeight, "Breakout by Gentlemen Doggo");
    SetTargetFPS(60);

    mainmenu::MainMenu* mainmenu = new mainmenu::MainMenu();
    mainmenu->play(false);
    delete mainmenu;


}