#include "OptionsMenu.h"
namespace optionsmenu {
	void OptionsMenu::init() {
		PlayMusicStream(sound::musicoptions);
		backmenu = false;
		_input = options::CHOISE::CREDITS;
		_background = LoadTexture("res/images/optionsmenu.png");
		Vector2 poscredit;
		Vector2 poscharacterselection;
		Vector2 posvolume;
		Vector2 posback;
		poscredit.x = 100;
		poscredit.y = 20;
		poscharacterselection.x = 100;
		poscharacterselection.y = 120;
		posvolume.x = 100;
		posvolume.y = 220;
		posback.x = 100;
		posback.y = 320;
		auxselection = 0;
		options[0] = new options::Options("CREDITS", poscredit);
		options[1] = new options::Options("PADDLE", poscharacterselection);
		options[2] = new options::Options("VOLUME", posvolume);
		options[3] = new options::Options("GOBACKOPTIONS", posback);

	}
	void OptionsMenu::play(PADDLESELECTION& paddleoptions,bool wins[]) {
		init();
		while(backmenu==false) {
			update();
			draw(paddleoptions,wins);
		}
	}
	void OptionsMenu::update() {
		UpdateMusicStream(sound::musicoptions);
		switch (_input)
		{
		case options::CHOISE::CREDITS:
			options[0]->optionSelection(_input);
			break;
		case options::CHOISE::CHARACTERSELECTION:
			options[1]->optionSelection(_input);
			break;
		case options::CHOISE::VOLUME:
			options[2]->optionSelection(_input);
			break;
		case options::CHOISE::GOBACKOPTIONS:
			options[3]->optionSelection(_input);
			if (IsKeyReleased(KEY_ENTER)) {
				backmenu = true;
			}
			break;
		}
	}
	void OptionsMenu::draw(PADDLESELECTION& paddleoptions, bool wins[]) {
		BeginDrawing();
		paddleoptions = (PADDLESELECTION)auxselection;
		ClearBackground(BLACK);
		DrawTexture(_background, 0, 0, WHITE);
		for (short i = 0;i < MAX_OPTIONS; i++) {
			options[i]->draw();
		}
		switch (_input)
		{
		case options::CHOISE::CREDITS:
			DrawText("Made By Gentlmen Doggo", 100, 400, 50, WHITE);
			break;
		case options::CHOISE::CHARACTERSELECTION:
			DrawText(">", 370, 120, 70, WHITE);
			if (IsKeyPressed(KEY_RIGHT)) {
				Color color=WHITE;
				paddleMenu(color,paddleoptions,wins);
			}
			break;
		case options::CHOISE::VOLUME:
			DrawText(">", 370, 220, 70, WHITE);
			if (IsKeyPressed(KEY_RIGHT)) {
				short input=5;
				volumenMenu(input);
			}
			break;
		}
		EndDrawing();
	}
	void OptionsMenu::paddleMenu(Color color,PADDLESELECTION& paddleoptions, bool wins[]) {
		player::Player* playermodel = new player::Player(1, 140);//1= level, 120=next to paddle button
		while (!IsKeyPressed(KEY_LEFT)) {
			UpdateMusicStream(sound::musicoptions);
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(_background, 0, 0, WHITE);
			for (short i = 0;i < MAX_OPTIONS; i++) {
				options[i]->draw();
			}
			DrawText("<", 370, 120, 70, RED);
			switch (paddleoptions)
			{
			case PADDLESELECTION::CWHITE:
				color = WHITE;
				if (IsKeyPressed(KEY_DOWN)) {
					auxselection++;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				break;
			case PADDLESELECTION::CBLUE:
				color = BLUE;
				if (IsKeyPressed(KEY_DOWN)) {
					auxselection++;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				if (IsKeyPressed(KEY_UP)) {
					auxselection--;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				break;
			case PADDLESELECTION::CRED:
				color = RED;
				if (IsKeyPressed(KEY_DOWN)) {
					auxselection++;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				if (IsKeyPressed(KEY_UP)) {
					auxselection--;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				break;
			case PADDLESELECTION::CGREEN:
				color = GREEN;
				if (IsKeyPressed(KEY_DOWN)) {
					auxselection++;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				if (IsKeyPressed(KEY_UP)) {
					auxselection--;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				break;
			case PADDLESELECTION::CYELLOW:
				color = YELLOW;
				if (IsKeyPressed(KEY_UP)) {
					auxselection--;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				if (wins[9] == true) {
					if (IsKeyPressed(KEY_DOWN)) {
						auxselection++;
						paddleoptions = (PADDLESELECTION)auxselection;
					}
				}
				break;
			case PADDLESELECTION::THEFORBIDDEN:
				color = playermodel->bossColor(10); //10= when boss on level 10 hass 10/10 lifes it shows this color
				if (IsKeyPressed(KEY_UP)) {
					auxselection--;
					paddleoptions = (PADDLESELECTION)auxselection;
				}
				break;
			}
			DrawRectangleRec(playermodel->getSizePos(), color);
			EndDrawing();
		}
		delete playermodel;
	}
	void OptionsMenu::volumenMenu(short& input) {
		while (!IsKeyPressed(KEY_LEFT)) {
			UpdateMusicStream(sound::musicoptions);
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(_background, 0, 0, WHITE);
			for (short i = 0;i < MAX_OPTIONS; i++) {
				options[i]->draw();
			}
			float auxvolumen[6]{ 0,0.05f,0.2f,0.3f,0.4f,0.5f };
			if (input < 5) {
				if (IsKeyPressed(KEY_UP)) {
					input++;
				}
			}
			else input = 5;
			if (input > 0) {

				if (IsKeyPressed(KEY_DOWN)) {
					input--;
				}

			}
			else input = 0;
			for (short i = 0;i < 5;i++) {
				if (input > i) {
					DrawRectangle(450 , 270-(i * 30), 10, 10, WHITE);
				}
			}
			DrawText("+", 435, 90, 70, RED);
			DrawText("-", 435, 275, 70, RED);
			sound::modificarVolumen(auxvolumen, input);
			DrawText("<", 370, 220, 70, RED);

			EndDrawing();
		}
	}
	
}