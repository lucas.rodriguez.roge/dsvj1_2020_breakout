#pragma once
#ifndef OPTIONSMENU_H
#define OPTIONSMENU_H
#define MAX_OPTIONS 4
#include "../../objects/Options/Options.h"
#include "../../objects/Player/Player.h"
#include "../../objects/PaddleSelection/PaddleSelection.h"
#include "../../objects/Sound/Sound.h"

namespace optionsmenu {
	class OptionsMenu {
	private:
		options::Options* options[MAX_OPTIONS];
		options::CHOISE _input;
		Texture2D _background;
		bool backmenu;
		short auxselection;
	public:
		void play(PADDLESELECTION & paddleoptions, bool wins[]);
		void init();
		void update();
		void draw(PADDLESELECTION& paddleoptions, bool wins[]);
		void paddleMenu(Color color,PADDLESELECTION& paddleoptions, bool wins[]);
		void volumenMenu(short& input);
	};
}
#endif // !OPTIONSMENU_H
