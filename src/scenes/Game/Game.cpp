#include "Game.h"

namespace game{
    void Game::play(short level, bool& win, PADDLESELECTION paddleoptions) {
        
        init(level,paddleoptions);
        while (gameover == false) {
            update(level,win,paddleoptions);
            draw(level,win,paddleoptions);
        }
        reinit();
    }
    void Game::init(short level, PADDLESELECTION paddleoptions) {
        background = LoadTexture("res/images/BackgroundGame.png");
        heart = LoadTexture("res/images/heart.png");
        PlayMusicStream(sound::musicgame);
        if (level == 10) {
            boss = new player::Player(level,50);
        }
        player = new player::Player(level,400);
        switch (paddleoptions) {
        case PADDLESELECTION::CWHITE:
            player->setColor(WHITE);
            break;
        case PADDLESELECTION::CBLUE:
            player->setColor(BLUE);
            break;
        case PADDLESELECTION::CRED:
            player->setColor(RED);
            break;
        case PADDLESELECTION::CGREEN:
            player->setColor(GREEN);
            break;
        case PADDLESELECTION::CYELLOW:
            player->setColor(YELLOW);
            break;
        }
        ball = new ball::Ball();
        pause = false;
        gameover = false;
        input = options::CHOISE::RESUME;
        pos_resume.x = 350;
        pos_resume.y = 150;
        pos_goback.x = 290;
        pos_goback.y = 250;
        options[0] = new options::Options("RESUME", pos_resume);
        options[1] = new options::Options("GOBACK", pos_goback);
        lifesboss = 10;
        lifesplayer = 5;
        Color cant_colors_blocks[5]{ WHITE,RED,BLUE,GREEN,YELLOW };
        short cant_blocks = 0;
        short cant_filas = 5;
        short aux_width = 60;
        short aux_height = 20;
        int aux_color = 0;
        //spawn block position
        if (level != 10) {
            for (short j = 10; j < aux_height * cant_filas; j += aux_height + 5) {
                for (short i = 10; i < screensize::screenWidth-aux_width+5; i += aux_width+5) {
                    if (cant_blocks < MAX_BLOCKS) {
                        block[cant_blocks] = new block::Block(i, j, aux_width, aux_height, cant_colors_blocks[aux_color]);
                        cant_blocks++;
                    }
                }
                aux_color++;

            }
        }
    }
    void Game::update(short level, bool& win,PADDLESELECTION paddleoptions) {
        UpdateMusicStream(sound::musicgame);
        if (pause == false) {
            player->movementControl(level);
            if (level == 10) {
                boss->setSizePosX(ball->getPosX()-boss->getSizePosWidth()/2);
                switch (lifesboss) {
                case 1:
                    player->setSizePosWidth(30);
                    break;
                case 2:
                    player->setSizePosWidth(35);
                    break;
                case 3:
                    player->setSizePosWidth(40);
                    break;
                case 4:
                    player->setSizePosWidth(50);
                    break;
                case 5:
                    player->setSizePosWidth(60);
                    break;
                case 6:
                    player->setSizePosWidth(70);
                    break;
                case 7:
                    player->setSizePosWidth(80);
                    break;
                case 8:
                    player->setSizePosWidth(90);
                    break;
                case 9:
                    player->setSizePosWidth(100);
                    break;
                case 10:
                    player->setSizePosWidth(150);
                }
            }
        collisionManager(level, win,paddleoptions);
        launchMechanic();
        }
        pauseMenu();

    }
    void Game::draw(short level, bool& win, PADDLESELECTION paddleoptions) {
        BeginDrawing();
        ClearBackground(BLACK);
        DrawTexture(background, 0, 0, WHITE);
        if (paddleoptions != PADDLESELECTION::THEFORBIDDEN) {
            player->draw();
        }
        else
            DrawRectangleRec(player->getSizePos(), player->bossColor(10));
        if (level != 10) {
            short blocksactivecounter = 0;
            for (short i = 0;i < MAX_BLOCKS;i++) {
                if (block[i]->getExist()) {
                    DrawRectangleRec(block[i]->getSizePos(), block[i]->getColor());
                    allblocksdown[i] = block[i]->getExist();
                }
                else {
                    allblocksdown[i] = block[i]->getExist();
                    if (allblocksdown[i] == false) {
                        blocksactivecounter++;
                    }
                }
            }
            if (blocksactivecounter == MAX_BLOCKS) {
                gameover = true;
                win = true;
            }
            for (short i = 0;i <= lifesplayer;i++) {
                if (lifesplayer >= 0)
                    DrawTexture(heart, i * 50, 420, RED);
            }
        }
        else if (lifesboss > 0) {
            DrawTexture(heart, 0, 420, RED);
            DrawRectangleRec(boss->getSizePos(), boss->bossColor(lifesboss));
        }
        else {
            gameover = true;
            win = true;
        }
        ball->draw();
        if (pause == true) {
            options[0]->draw();
            options[1]->draw();
        }
        EndDrawing();
    }
    void Game::reinit() {
        delete player;
        delete ball;
        for (short i = 0;i < MAX_BLOCKS;i++) {
            if (block[i] != NULL) {
                delete block[i];
            }
        }
        UnloadTexture(background);

    }
    void Game::collisionManager(short level, bool& win, PADDLESELECTION paddleoptions) {
        // Collision logic: ball vs blocks 
        if (level != 10) {
            Vector2 p= Vector2();
            for (int j = 0; j < MAX_BLOCKS; j++)
            {
                if (block[j]->getExist() == true)
                {
                    p = getPaddlePerimeterPosition(block[j]->getSizePos());
                    if (checkCollisionPaddle(p)) {
                        blocksLifes(j);
                        PlaySound(sound::blockbeep);
                        changeDirection(p, block[j]->getSizePos());
                    }
                    /*
                    // Hit below
                    if (((ball->getPosY() - ball->getRadius()) <= (block[j]->getSizePosY() + block[j]->getSizePosHeight())) &&
                        ((ball->getPosY() - ball->getRadius()) > (block[j]->getSizePosY() + block[j]->getSizePosHeight()/2 + ball->getSpeedY()) &&
                            ((fabs((double)ball->getPosX() - (double)block[j]->getSizePosX()))) < ((double)block[j]->getSizePosWidth() / 2 + (double)ball->getRadius() * 2 / 3)) && (ball->getSpeedY() < 0))
                    {
                        blocksLifes(j);
                        ball->setSpeedY(ball->getSpeedY() * -1.0f);
                    }
                    // Hit above
                    else if (((ball->getPosY() + ball->getRadius()) >= (block[j]->getSizePosY() - block[j]->getSizePosHeight() / 2)) &&
                        ((ball->getPosY() + ball->getRadius()) < (block[j]->getSizePosY() - block[j]->getSizePosHeight() / 2 + ball->getSpeedY())) &&
                        ((fabs((double)ball->getPosX() - (double)block[j]->getSizePosX())) < ((double)block[j]->getSizePosWidth() / 2 + (double)ball->getRadius() * 2 / 3)) && (ball->getSpeedY() > 0))
                    {
                        blocksLifes(j);
                        ball->setSpeedY(ball->getSpeedY() * -1.0f);
                    }
                    // Hit left
                    else if (((ball->getPosX() + ball->getRadius()) >= (block[j]->getSizePosX() - block[j]->getSizePosHeight() / 2)) &&
                        ((ball->getPosX() + ball->getRadius()) < (block[j]->getSizePosX() - block[j]->getSizePosHeight() / 2 + ball->getSpeedX())) &&
                        ((fabs((double)ball->getPosY() - (double)block[j]->getSizePosY())) < ((double)block[j]->getSizePosWidth() / 2 + (double)ball->getRadius() * 2 / 3)) && (ball->getSpeedX() > 0))
                    {
                        blocksLifes(j);
                        ball->setSpeedX(ball->getSpeedX() * -1.0f);
                    }
                    // Hit right
                    else if (((ball->getPosX() - ball->getRadius()) <= (block[j]->getSizePosX() + block[j]->getSizePosHeight() / 2)) &&
                        ((ball->getPosX() - ball->getRadius()) > (block[j]->getSizePosX() + block[j]->getSizePosHeight() / 2 + ball->getSpeedX())) &&
                        ((fabs((double)ball->getPosY() - (double)block[j]->getSizePosY())) < ((double)block[j]->getSizePosWidth() / 2 + (double)ball->getRadius() * 2 / 3)) && (ball->getSpeedX() < 0))
                    {
                        blocksLifes(j);
                        ball->setSpeedX(ball->getSpeedX() * -1.0f);
                    }
                    */
                }
            }
        }
        // Collision logic: ball vs walls 
        if ((ball->getPosX() + ball->getRadius()) >= (double)screensize::screenWidth) {
            ball->setSpeedX((ball->getSpeedX() * -1.0f));
            PlaySound(sound::playerbeep);
        }
        if ((ball->getPosX() - ball->getRadius()) <= 0.0f) {
            ball->setSpeedX((ball->getSpeedX() * -1.0f));
            PlaySound(sound::playerbeep);
        }
        if ((ball->getPosY() - ball->getRadius()) <= 0.0f) {
            ball->setSpeedY(ball->getSpeedY() * -1.0f);
            PlaySound(sound::playerbeep);
        }
        /*
        // Collision logic: ball vs corners if fps drops
        if (((ball->getPosX() + ball->getRadius()) >= screensize::screenWidth) && ((ball->getPosY() - ball->getRadius()) <= 0)) {
            ball->setSpeedX((ball->getSpeedX() * -1.0f));
            ball->setSpeedY(ball->getSpeedY() * -1.0f);
        }
        if (((ball->getPosX() + ball->getRadius()) >= screensize::screenWidth) && (CheckCollisionCircleRec(ball->getPos(), ball->getRadius(), player->getSizePos()) == true)) {
            ball->setSpeedX((ball->getSpeedX() * -1.0f));
            ball->setSpeedY(ball->getSpeedY() * -1.0f);
        }
        if (((ball->getPosX() - ball->getRadius()) <= 0)&& (CheckCollisionCircleRec(ball->getPos(), ball->getRadius(), player->getSizePos()) == true)){
            ball->setSpeedX((ball->getSpeedX() * -1.0f));
            ball->setSpeedY(ball->getSpeedY() * -1.0f);
        }
        if (((ball->getPosX() - ball->getRadius()) <= 0) && ((ball->getPosY() - ball->getRadius()) <= 0)) {
            ball->setSpeedX((ball->getSpeedX() * -1.0f));
            ball->setSpeedY(ball->getSpeedY() * -1.0f);
        }
        */



        //Collision logic: ball vs bottom screen
        if ((ball->getPosY() + ball->getRadius()) >= screensize::screenHeight)   {
            ball->setSpeed({ 0.0f,0.0f });
            if (lifesplayer > 0&&level!=10) {
                lifesplayer--;
                ball->setActive(false);
            }
            else {
                gameover = true;
            }

            win = false;
        }
        
        

        // Collision logic: ball vs player
        if (CheckCollisionCircleRec(ball->getPos(), ball->getRadius(), player->getSizePos()))
        {

            if (ball->getSpeedY() > 0)
            {
               
                ball->setSpeedY(ball->getSpeedY() * -1.0f);
                short half = (player->getSizePosX() + player->getSizePosWidth() / 2);
                if (ball->getPosX() < half) {// parte izquierda del paddle
                    ball->setSpeedX((((double)player->getSizePosX() + (double)player->getSizePosWidth()) - ball->getPosX()) / ((double)player->getSizePosWidth()) * -200.0f);
                    if (paddleoptions != PADDLESELECTION::THEFORBIDDEN)
                        PlaySound(sound::playerbeep);
                    else
                        PlaySound(sound::bossbeep);
                }
                else { //parte derecha del paddle
                    ball->setSpeedX((ball->getPosX() - (double)player->getSizePosX()) / ((double)player->getSizePosWidth()) * 200.0f);
                    if (paddleoptions != PADDLESELECTION::THEFORBIDDEN)
                        PlaySound(sound::playerbeep);
                    else
                        PlaySound(sound::bossbeep);
                }
               
            }
        }
        //Collision logic: ball vs boss
        if (level == 10&&lifesboss>0) {       
            if (CheckCollisionCircleRec(ball->getPos(), ball->getRadius(), boss->getSizePos()))
            {
                ball->setSpeedY(ball->getSpeedY() * -1.0f);
                short half = (boss->getSizePosX() + boss->getSizePosWidth() / 2);
                if (ball->getPosX() < half) {// parte izquierda del paddle
                    PlaySound(sound::bossbeep);
                    ball->setSpeedX((((double)boss->getSizePosX() + (double)boss->getSizePosWidth()) - ball->getPosX()) / ((double)boss->getSizePosWidth()) * -200);
                }
                else { //parte derecha del paddle
                    PlaySound(sound::bossbeep);
                    ball->setSpeedX((ball->getPosX() - (double)boss->getSizePosX()) / ((double)boss->getSizePosWidth()) * 200);
                }
                lifesboss--;
                ball->setSpeed({ (float)ball->getSpeedX() * 1.2f,(float)ball->getSpeedY() * 1.2f});
            }
        }


    }
    void Game::pauseMenu() {
        if (IsKeyPressed(KEY_P)) {
            pause = true;
        }
        if (pause == true) {
            switch (input) {
            case options::CHOISE::RESUME:
                options[0]->optionSelection(input);
                if (IsKeyPressed(KEY_ENTER)) 
                pause = false;
                break;
            case options::CHOISE::GOBACK:              
                options[1]->optionSelection(input);
                if (IsKeyReleased(KEY_ENTER)) {
                    gameover=true;
                }
                break;
            }
        }
    }
    void Game::unloadTextures() {
        UnloadTexture(background);
    }
    bool Game::getPause() {
        return pause;
    }
    void Game::setPause(bool _pause) {
        pause = _pause;
    }
    void Game::launchMechanic() {
        if (!ball->getActive())
        {
            if (IsKeyPressed(KEY_SPACE))
            {
                ball->setActive(true);
                ball->setSpeed({ 200, -200 });
            }
        }

        // Ball movement logic
        if (ball->getActive())
        {
            ball->setPosX(ball->getPosX() + ball->getSpeedX()* GetFrameTime());
            ball->setPosY(ball->getPosY() + ball->getSpeedY()* GetFrameTime());
        }
        else
        {
            ball->setPos({ (player->getSizePosX() + (player->getSizePosWidth() / 2)), (player->getSizePosY() - player->getSizePosHeight() / 2) });
        }
    }
    void Game::blocksLifes(short j) {
        if (ColorToInt(block[j]->getColor()) == ColorToInt(GREEN))block[j]->setExist(false);
        else if (ColorToInt(block[j]->getColor()) == ColorToInt(BLUE))block[j]->setColor(GREEN);
        else if (ColorToInt(block[j]->getColor()) == ColorToInt(RED))block[j]->setColor(BLUE);
        else if (ColorToInt(block[j]->getColor()) == ColorToInt(WHITE))block[j]->setColor(RED);
    }
    void Game::changeDirection(Vector2 p, Rectangle paddle)
    {
            //p=ball paddle=block
            //Collision Up
            if (ball->getPosY() <= paddle.y)
            {
                ball->setPosY(paddle.y - ball->getRadius());
                ball->setSpeedY(ball->getSpeedY() * -1.0f);

                return;
            }

            //Collision Down
            if (ball->getPosY() >= paddle.y + paddle.height)
            {
                ball->setPosY(paddle.y + ball->getRadius()+ paddle.height);
                ball->setSpeedY(ball->getSpeedY() * -1.0f);

                return;
            }

            //Collision Left
            if (ball->getPosX() <= paddle.x)
            {
                ball->setPosX(paddle.x - ball->getRadius() - 1.0f);
                ball->setSpeedX(ball->getSpeedX() * -1.0f);
                return;
            }

            //Collision Right
            if (ball->getPosX() >= paddle.x + paddle.width)
            {
                ball->setPosX(paddle.x + paddle.width + ball->getRadius() - 1);
                ball->setSpeedX(ball->getSpeedX() * -1.0f);
                return;
            }
    }
    bool Game::checkCollisionPaddle(Vector2 p)
    {
        double distance = sqrt((static_cast<double>(ball->getPosX()) - static_cast<double>(p.x)) * (static_cast<double>(ball->getPosX()) - static_cast<double>(p.x)) + (static_cast<double>(ball->getPosY()) - static_cast<double>(p.y)) * (static_cast<double>(ball->getPosY()) - static_cast<double>(p.y)));
        if (distance < ball->getRadius())
        {
            return true;
        }

        return false;
    }
    Vector2 Game::getPaddlePerimeterPosition(Rectangle paddle)
    {
        Vector2 p = Vector2();

        p.x = ball->getPosX();
        if (p.x < paddle.x)
        {
            p.x = paddle.x;
        }
        else if (p.x > paddle.x + paddle.width)
        {
            p.x = paddle.x + paddle.width;
        }

        p.y = ball->getPosY();
        if (p.y < paddle.y)
        {
            p.y = paddle.y;
        }
        else if (p.y > paddle.y + paddle.height)
        {
            p.y = paddle.y + paddle.height;
        }

        return p;
    }
}