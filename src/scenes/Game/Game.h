#pragma once
#ifndef GAME_H
#define GAME_H

#define MAX_BLOCKS 48
#define MAX_OPTIONS 2
#define CANT_COLORS 5
#include "../../objects/Player/Player.h"
#include "../../objects/Block/Block.h"
#include "../../objects/Ball/Ball.h"
#include "../../objects/Options/Options.h"
#include "../../objects/PaddleSelection/PaddleSelection.h"
#include "../../ScreenSize.h"
#include "../../objects/Sound/Sound.h"
#include "../LevelSelection/LevelSelection.h"
#include "../OptionsMenu/OptionsMenu.h"

#include <cmath>


namespace game{

	class Game {
	private:
		player::Player* player;
		player::Player* boss;
		ball::Ball* ball;
		block::Block* block[MAX_BLOCKS];
		options::Options* options[MAX_OPTIONS];
		options::CHOISE input;
		Vector2 pos_resume;
		Vector2 pos_goback;
		bool gameover;
		bool pause;
		Texture2D background;
		short lifesboss;
		short lifesplayer;
		Texture2D heart;
		bool allblocksdown[MAX_BLOCKS];

	public:
		void init(short level, PADDLESELECTION paddleoptions);
		void update(short level,bool& win, PADDLESELECTION paddleoptions);
		void draw(short level, bool& win, PADDLESELECTION paddleoptions);
		void reinit();
		void collisionManager(short level,bool& win, PADDLESELECTION paddleoptions);
		void play(short level,bool& win,PADDLESELECTION paddleoptions);
		void pauseMenu();
		void unloadTextures();
		bool getPause();
		void setPause(bool _pause);
		void launchMechanic();
		void blocksLifes(short j);
		void changeDirection(Vector2 p, Rectangle paddle);
		bool checkCollisionPaddle(Vector2 p);
		Vector2 getPaddlePerimeterPosition(Rectangle paddle);
		
	};
}

#endif // GAME_H
