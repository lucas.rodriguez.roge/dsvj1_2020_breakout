#pragma once
#ifndef LEVELSELECION_H
#define LEVELSELECTION_H
#define MAX_LEVELS 11
#include "../../objects/Options/Options.h"
#include "../Game/Game.h"
#include "../MainMenu/MainMenu.h"
#include "../../objects/PaddleSelection/PaddleSelection.h"

namespace levelselection {
	class LevelSelection {
	private:
		options::Options* options[MAX_LEVELS];
		options::CHOISE _input;
		Texture2D _background;
		Texture2D _star;
		bool backmenu;
	public:
		void init();
		void update(PADDLESELECTION paddleoptions, bool wins[]);
		void draw(bool wins[]);
		void play(PADDLESELECTION paddleoptions, bool wins[]);
	};
}
#endif // !LEVELSELECION_H
