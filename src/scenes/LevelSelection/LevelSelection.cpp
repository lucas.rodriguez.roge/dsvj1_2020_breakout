#include "LevelSelection.h"

namespace levelselection {
    
    void LevelSelection::init() {
        PlayMusicStream(sound::musiclevelselection);
        for (short i = 0;i < MAX_LEVELS;i++) {
            options[i] = new options::Options(FormatText("%d", i+1), { ((float)i * 80),250 });
            if (i == 10) {
                options[i] = new options::Options("GOBACKLEVEL", {25,375 });
            }
        }
        _input = options::CHOISE::LEVEL1;
        _background = LoadTexture("res/images/levelselectionmenu.png");
        _star = LoadTexture("res/images/star1.png");
        backmenu = false;
    }
    void LevelSelection::update(PADDLESELECTION paddleoptions,bool wins[]) {
        UpdateMusicStream(sound::musiclevelselection);
        switch (_input) {
        case options::CHOISE::LEVEL1:
            options[0]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level1 = new game::Game();
                level1->play(1,wins[0],paddleoptions);
                delete level1;
            }
            
            break;
        case options::CHOISE::LEVEL2:
            options[1]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level2 = new game::Game();
                level2->play(2, wins[1], paddleoptions);
                delete level2;
            }
            break;
        case options::CHOISE::LEVEL3:
            options[2]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level3 = new game::Game();
                level3->play(3, wins[2], paddleoptions);
                delete level3;
            }
            break;
        case options::CHOISE::LEVEL4:
            options[3]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level4 = new game::Game();
                level4->play(4, wins[3], paddleoptions);
                delete level4;
            }
            break;
        case options::CHOISE::LEVEL5:
            options[4]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level5 = new game::Game();
                level5->play(5, wins[4], paddleoptions);
                delete level5;
            }
            break;
        case options::CHOISE::LEVEL6:
            options[5]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level6 = new game::Game();
                level6->play(6, wins[5], paddleoptions);
                delete level6;
            }
            break;
        case options::CHOISE::LEVEL7:
            options[6]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level7 = new game::Game();
                level7->play(7, wins[6], paddleoptions);
                delete level7;
            }
            break;
        case options::CHOISE::LEVEL8:
            options[7]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level8 = new game::Game();
                level8->play(8, wins[7], paddleoptions);
                delete level8;
            }
            break;
        case options::CHOISE::LEVEL9:
            options[8]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level9 = new game::Game();
                level9->play(9, wins[8], paddleoptions);
                delete level9;
            }
            break;
        case options::CHOISE::LEVEL10:
            options[9]->optionSelection(_input);
            if (IsKeyPressed(KEY_ENTER)) {
                game::Game* level10 = new game::Game();
                level10->play(10, wins[9], paddleoptions);
                delete level10;
            }
            break;
        case options::CHOISE::GOBACKLEVEL:
            options[10]->optionSelection(_input);
            if (IsKeyReleased(KEY_ENTER)) {
                backmenu = true;
            }
            break;
        }
    }
    void LevelSelection::draw(bool wins[]) {
        BeginDrawing();
        ClearBackground(BLACK);
        DrawTexture(_background, 0, 0, WHITE);
        for (short i = 0;i < MAX_LEVELS - 1;i++) {
            if (wins[i] == true) {
                DrawTexture(_star, i * 80, 300, WHITE);
            }
        }
        for (short i = 0;i < MAX_LEVELS;i++) {
            options[i]->draw();
        }
        EndDrawing();
    }
    void LevelSelection::play(PADDLESELECTION paddleoptions, bool wins[]) {
       init();
        while (backmenu==false) {
            update(paddleoptions,wins);
            draw(wins);
        }
    }
}
