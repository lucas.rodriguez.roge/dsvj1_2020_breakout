#pragma once
#ifndef MAINMENU_H
#define MAINMENU_H
#define MAXOPTIONS 3
#define LEVELS 10

#include "../../objects/Title/Title.h"
#include "../../objects/Options/Options.h"
#include "../../objects/PaddleSelection/PaddleSelection.h"
#include "../../ScreenSize.h"
#include "../../objects/Sound/Sound.h"
#include "../LevelSelection/LevelSelection.h"
#include "../OptionsMenu/OptionsMenu.h"


enum class SCENES{MAINMENU,OPTIONS,GAME};

namespace mainmenu {
	class MainMenu {
	private:
		options::Options* _options[MAXOPTIONS];
		title::Title* _title;
		options::CHOISE _input;
		Texture2D _background;
		bool _exit;
		Color selection;
		bool wins[LEVELS];
		PADDLESELECTION paddleoptions;
	public:
		void init();
		void update();
		void draw();
		void play(bool exit);
		void reinit();

	};
}
#endif // MAINMENU_H