#include "raylib.h"
#include "MainMenu.h"
  

namespace mainmenu {
    
    void MainMenu::play(bool exit) {
        init();

        while (!exit)    // Detect window close button or ESC key
        {
            update();
            draw();
        }
        reinit();
        CloseWindow();
    }
    void MainMenu::init() {
        sound::initSounds();
        PlayMusicStream(sound::musicmenu);
        _exit = false;
        _background = LoadTexture("res/images/fondo2.png");
        paddleoptions = PADDLESELECTION::CWHITE;
        _title = new title::Title("Arcanoid");
        _title->init();

        Vector2 pos_play;
        Vector2 pos_options;
        Vector2 pos_exit;

        _input = options::CHOISE::OPTIONS;
        pos_play.x = 330;
        pos_play.y = 150;
        pos_options.x = 330;
        pos_options.y = 250;
        pos_exit.x = 330;
        pos_exit.y = 350;
        _options[0] = new options::Options("PLAY", pos_play);
        _options[1] = new options::Options("OPTIONS", pos_options);
        _options[2] = new options::Options("EXIT", pos_exit);
    }
    void MainMenu::update() {
        UpdateMusicStream(sound::musicmenu);
        switch (_input) {
        case options::CHOISE::PLAY:
            _options[0]->optionSelection(_input);
            if (IsKeyReleased(KEY_ENTER)) {
                levelselection::LevelSelection* levelselection = new levelselection::LevelSelection();
                levelselection->play(paddleoptions,wins);
                delete levelselection;
            }
            break;
        case options::CHOISE::OPTIONS:
            _options[1]->optionSelection(_input);
            if (IsKeyReleased(KEY_ENTER)) {
                optionsmenu::OptionsMenu* optionsmenu = new optionsmenu::OptionsMenu();
                optionsmenu->play(paddleoptions,wins);
                delete optionsmenu;
            }
            break;
        case options::CHOISE::EXIT:
            if (IsKeyPressed(KEY_ENTER)) {
                mainmenu::MainMenu* exit = new mainmenu::MainMenu();
                exit->play(true);
                delete exit;
            }
            _options[2]->optionSelection(_input);
            break;
        }


    }
    void MainMenu::draw() {
        BeginDrawing();
        ClearBackground(BLACK);
        DrawTexture(_background, 0, 0, WHITE);
        _title->draw();
        for (short i = 0;i < MAXOPTIONS; i++) {
            _options[i]->draw();
        }
        EndDrawing();
    }
    void MainMenu::reinit() {
        UnloadTexture(_background);
        CloseAudioDevice();
    }
}
